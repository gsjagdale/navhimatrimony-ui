import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ListProfileComponent } from "./profile/list/list-profile.component";
import { LoginComponent } from "./other/login/login.component";
import { MenubarComponent } from "./other/menubar/menubar.component";
import { PageNotFoundComponent } from "./other/pagenotfound.component";
import { ContactUsComponent } from "./other/contactus/contactus.component";
import { AboutUsComponent } from "./other/aboutus/aboutus.component";
import { ProfileDetailComponent } from './profile/detail/detail.component';
import { ProfileAddComponent } from './profile/add/add.component';

@NgModule({
  declarations: [
    AppComponent,
    ListProfileComponent,
    LoginComponent,
    MenubarComponent,
    PageNotFoundComponent,
    ContactUsComponent,
    AboutUsComponent,
    ProfileDetailComponent,
    ProfileAddComponent,
    ProfileDetailComponent
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PageNotFoundComponent } from "./other/pagenotfound.component";
import { LoginComponent } from "./other/login/login.component";
import { ContactUsComponent } from "./other/contactus/contactus.component";
import { ListProfileComponent } from "./profile/list/list-profile.component";
import { AboutUsComponent } from "./other/aboutus/aboutus.component";
import { ProfileAddComponent } from "./profile/add/add.component";
import { ProfileDetailComponent } from "./profile/detail/detail.component";

const routes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "contactus", component: ContactUsComponent },
  { path: "addprofile", component: ProfileAddComponent },
  { path: "detailprofile", component: ProfileDetailComponent },
  { path: "listprofile", component: ListProfileComponent },
  { path: "aboutus", component: AboutUsComponent },
  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

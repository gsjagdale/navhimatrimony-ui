import { Component, OnInit } from "@angular/core";
import { isNumber } from "util";

@Component({
  selector: "app-list-profile",
  templateUrl: "./list-profile.component.html",
  styleUrls: ["./list-profile.component.css"]
})
export class ListProfileComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  getAge(dob : string) {
    if (!isNumber(dob)) {
      return 0;
    }
    let diff: number =
      new Date().getMilliseconds() - new Date(dob).getMilliseconds();
    return diff / 31536000000;
  }

  data: any[] = [
    {
      id: 702,
      address: {
        state: "Maharashtra",
        city: "Parli-Vaijnath",
        addr1: "E 76/455, Shakti-Kunj",
        addr2: "TPS COlony",
        district: "Beed"
      },
      height: 168,
      color: "Wheatish",
      birthPlace: "Ter, Osmanabad",
      gender: "Male",
      education: "B.E. Computer Sc. & Engg",
      occupation: "Software Engg. Cognizant",
      dob: "kl",
      fName: "Shrikar",
      mName: "Subodh",
      lName: "Jagdale",
      aboutme:
        "Done Engineering from Osmanabad. Working with Cognizant since 6 Yrs.",
      expectation: "Well educated/graduate. Ready to do service. Understanding."
    }
  ];
}

import { Component, OnInit } from "@angular/core";

@Component({
  selector: "profile-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.css"]
})
export class ProfileAddComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  months: String[] = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  days: Number[] = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31
  ];
  /*$scope.days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24];*/
  districts: String[] = ["Osmanabad", "Pune"];
  colors: String[] = ["Gora", "Kala", "Wheatish", "White"];
  years: number[] = [1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000];
  maritalStatuses: String[] = ["Single", "Divorcee", "Widow/Widower"];

  genders: String[] = ["Male", "Female"];
}
